import React, {useState} from 'react';
import './App.css';
import {Nav, Navbar} from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import Routes from "./Routes";
import { AppContext } from "./lib/contextLib";

function App() {
  const [isAuthenticated, userHasAuthenticated] = useState(false);

  const handleLogout = () => {
    userHasAuthenticated(false);
  }

  return (
    <div className="App container py-3" data-bs-theme="dark">
      <Navbar collapseOnSelect expand="md" className="mb-3 px-3" data-bs-theme="dark">
        <Navbar.Brand className="fw-bold" href="/">Lunmoji</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Nav activeKey={window.location.pathname}>
            {isAuthenticated ? (
              <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
            ) : (
              <>
                <LinkContainer to="/signup">
                  <Nav.Link>Signup</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/login">
                  <Nav.Link>Login</Nav.Link>
                </LinkContainer>
              </>
            )}
            <Nav.Link href="lunmoji.zip">Download</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <AppContext.Provider value={{ isAuthenticated, userHasAuthenticated }}>
        <Routes />
      </AppContext.Provider>
    </div>
  );
}

export default App;
