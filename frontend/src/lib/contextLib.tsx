import { useContext, createContext } from "react";

interface IAppContext {
  isAuthenticated: boolean;
  userHasAuthenticated: React.Dispatch<React.SetStateAction<boolean>>;
}

const defaultContext: IAppContext = ({} as any) as IAppContext;

export const AppContext = createContext(defaultContext);

export function useAppContext() {
  return useContext(AppContext);
}